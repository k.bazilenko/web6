const db = {
    "Iphone7Model": {
        name: "Iphone 7",
        price: 49990,
        variants: {},
        additional: {}
    },

    "IphoneXModel": {
        name: "Iphone X",
        price: 69990,
        variants: {
            "64 Gb": 0,
            "128 Gb": 10000,
            "256 Gb": 20000
        },
        additional: {}
    },

    "Iphone11Model": {
        name: "Iphone 11",
        price: 79990,
        variants: {},
        additional: {
            "Calls": 3000,
            "Messages": 2500
        }
    }
};

let state = {
    isError: false,
    price: db.Iphone7Model.price,
    variantsPrice: 0,
    additionalPrice: 0,
    count: 0,
    notify() {
        let totalAmount = document.getElementById("totalAmount");
        if (this.isError) {
            totalAmount.value = "ERROR";
        } else {
            totalAmount.value = this.calcTotalAmount() + "$";
        }
    },
    calcTotalAmount() {
        let resultPrice = this.price;
        resultPrice += this.variantsPrice;
        resultPrice += this.additionalPrice;
        return this.count * resultPrice;
    }
};

function countInputListener(event) {
    if (!event.target.value.match(/^[0-9]+$/) && event.target.value !== "") {
        state.isError = true;
        state.notify();
        return;
    }
    state.count = parseInt(event.target.value);
    if (state.count < 0) {
        state.isError = true;
        state.notify();
        return;
    }
    state.isError = false;
    state.count = event.target.value;
    state.notify();
}

function setVariantsForModel(IphoneModel) {
    let variantsRadioButtons = document.getElementById("variantsRadioButtons");
    variantsRadioButtons.style.display = "none";
    variantsRadioButtons.innerHTML = "";
    Object.keys(db[IphoneModel].variants).forEach(function (variantKey) {
        variantsRadioButtons.innerHTML +=
        `<span class="label label-default"><input name="modelVariant" ` +
        `onchange={variantSelectionListener(event)} ` +
        `type="radio" ` +
        `style="width: 15px; height: 15px; margin-right: 10px;" ` +
        `value="${db[IphoneModel].variants[variantKey]}">` +
        `${variantKey}` +
        `</span><br>`;
    });
    if (variantsRadioButtons.innerHTML !== "") {
        variantsRadioButtons.innerHTML = `Выберите вариант ` +
        `${db[IphoneModel].name}:<br/>` +
        variantsRadioButtons.innerHTML;
    }
    variantsRadioButtons.style.display = "block";
}

function setAdditionalForModel(IphoneModel) {
    let additionalCheckboxes = document.getElementById("additionalCheckboxes");
    additionalCheckboxes.style.display = "none";
    additionalCheckboxes.innerHTML = "";
    Object.keys(db[IphoneModel].additional).forEach(function (additionalKey) {
        additionalCheckboxes.innerHTML +=
        `<span><input name="modelAdditional" ` +
        `onchange={additionalSelectionListener(event)} ` +
        `type="checkbox" ` +
        `style="width: 15px; height: 15px; margin-right: 10px;" ` +
        `value="${db[IphoneModel].additional[additionalKey]}"/>` +
        `${additionalKey}` +
        `</span></br>`;
    });
    if (additionalCheckboxes.innerHTML !== "") {
        additionalCheckboxes.innerHTML = `Выберите опции ` +
        `${db[IphoneModel].name}:<br/>` +
        additionalCheckboxes.innerHTML;
    }
    additionalCheckboxes.style.display = "block";
}

function modelSelectionListener(event) {
    state.price = parseInt(db[event.target.value].price);
    state.variantsPrice = 0;
    state.additionalPrice = 0;
    setVariantsForModel(event.target.value);
    setAdditionalForModel(event.target.value);
    state.notify();
}

function variantSelectionListener(event) {
    state.variantsPrice = parseInt(event.target.value);
    state.notify();
}

function additionalSelectionListener(event) {
    if (event.target.checked) {
        state.additionalPrice += parseInt(event.target.value);
    } else {
        state.additionalPrice -= parseInt(event.target.value);
    }
    state.notify();
}

window.addEventListener("DOMContentLoaded", function () {
    let IphoneCountInput = document.getElementById("IphoneCountInput");
    IphoneCountInput.addEventListener("input", countInputListener);
    let modelVariant = document.getElementById("IphoneModelSelect");
    modelVariant.addEventListener("change", modelSelectionListener);
    setVariantsForModel("Iphone7Model");
    setAdditionalForModel("Iphone7Model");
});
